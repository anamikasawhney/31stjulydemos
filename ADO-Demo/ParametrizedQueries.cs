﻿using System.Data.SqlClient;

namespace ADO_Demo
{
    internal class ParametrizedQueries
    {
        static SqlConnection connection = null;
        static SqlCommand command = null;
        static string GetConnectionString()
        {
            return @"data source=ANAMIKA\SQLSERVER;initial catalog=practiceDb;integrated security=true";

        }
        static SqlConnection GetConnection()
        {
            connection = new SqlConnection(GetConnectionString());
            return connection;
        } 
        public static void Main(string[] args)
            {
                string choice = "y";
                while (choice == "y")
                {
                    int ch = MainMenu();
                    switch (ch)
                    {

                        // File Handling > FileStrem fs ;  fs.Close(); /. close method will 
                        // destroy / dispose / remove fs object from memory

                        case 1:
                            {
                                try
                                {
                                    connection = GetConnection();
                                command = new SqlCommand();
                                Console.WriteLine("Enter Name");
                                string name = Console.ReadLine();
                                Console.WriteLine("Enter Address");
                                string address = Console.ReadLine();
                                Console.WriteLine("Enter Salary");
                                int salary = int.Parse(Console.ReadLine());

                                Console.WriteLine("Enter DOJ");
                                DateTime doj = Convert.ToDateTime(Console.ReadLine());
                                Console.WriteLine("Are you in tax slab");
                                bool tax_slab = bool.Parse(Console.ReadLine());
                                 command.CommandText = "Insert into Employee values(@name,@address, @salary,@doj,@tax_slab)";
                                    command.Connection = connection;
                                command.Parameters.AddWithValue("@name", name);
                                command.Parameters.AddWithValue("@address", address);
                                command.Parameters.AddWithValue("@salary", salary);
                                command.Parameters.AddWithValue("@doj", doj);
                                command.Parameters.AddWithValue("@tax_slab", tax_slab);
                                    connection.Open();
                                    command.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                                finally
                                {

                                    connection.Close();
                                    command.Dispose();  // This methiod will remove command object
                                    connection.Dispose(); // This methiod will remove connection object
                                }
                                break;
                            }
                        case 2:
                            {
                                try
                                {
                                    connection = GetConnection();
                                command = new SqlCommand();
                                command = new SqlCommand("update employee set address='Bombay' where id=2", connection);
                                    connection.Open();
                                    command.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                                finally
                                {
                                    connection.Close();
                                    command.Dispose();
                                    connection.Dispose();
                                }
                                break;
                            }
                        case 3:
                            {
                                try
                                {
                                    connection = GetConnection();
                                command = new SqlCommand();
                                command = new SqlCommand("delete Employee where id=2", connection);
                                    connection.Open();
                                    command.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                                finally
                                {
                                    connection.Close();
                                    command.Dispose();
                                    connection.Dispose();
                                }
                                break;
                            }
                        case 4:
                            {
                                try
                                {
                                    connection = GetConnection();
                                command = new SqlCommand();
                                command = new SqlCommand("select * from employee where id=3", connection);
                                    connection.Open();
                                    SqlDataReader reader = command.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        Console.WriteLine(reader[0] + " " + reader[1] + " " + reader[2]);

                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                                finally
                                {
                                    connection.Close();
                                    command.Dispose();
                                    connection.Dispose();
                                }
                                break;
                            }
                        case 5:
                            {
                                try
                                {
                                    connection = GetConnection();
                                command = new SqlCommand();
                                command = new SqlCommand("select * from employee", connection);
                                    connection.Open();
                                    string str = "";
                                    SqlDataReader reader = command.ExecuteReader();
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            for (int i = 0; i < reader.FieldCount; i++)
                                            {

                                                str += reader[i] + " ";

                                            }
                                            Console.WriteLine(str);
                                            str = "";
                                        }
                                        //while(reader.Read())
                                        //{
                                        //    Console.WriteLine($"{ reader["id"]}  {reader["name"]}");
                                        //}



                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                                finally
                                {
                                    connection.Close();
                                    command.Dispose();
                                    connection.Dispose();
                                }
                                break;
                            }
                        default: Console.WriteLine("Invalid choice"); break;
                    }
                Console.WriteLine("Want to repeat");
                choice = Console.ReadLine();
                }
            }
        
        private static int MainMenu()
        {
            Console.WriteLine("1. Insert Record");
            Console.WriteLine("2. Edit Record");
            Console.WriteLine("3. Delete Record");
            Console.WriteLine("1. Search Record");
            Console.WriteLine("1. List all Employees");
            Console.WriteLine("Enter your choice");
            return Byte.Parse(Console.ReadLine());
        }
    }
}
