﻿using System.Data.SqlClient;

namespace ADO_Demo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //// Step 1: 
            //string connectionString = @"data source=ANAMIKA\SQLSERVER;initial catalog=practiceDb;integrated security=true";
            //SqlConnection connection = new SqlConnection(connectionString);
            //// Step 2:
            
            //SqlCommand sqlCommand = new SqlCommand();
            //sqlCommand.CommandText = "Select * from Employee";
            //sqlCommand.Connection = connection;

            //// Step 3: 
            //// Opn connection

            //connection.Open(); // connection establishes between appl & database

            // Step 4 : Run the command , the command will be executed 
            // on sql server
            // Get Records> ExecuteReader()
            // Perform insert/ dlete / update > ExecuteNonQuery()

            // Step5 > CLose connection // Setp6 Destroy the connection & command object

            string choice = "y";
            while (choice == "y")
            {
                int ch = MainMenu();
                switch (ch) {

                    // File Handling > FileStrem fs ;  fs.Close(); /. close method will 
                    // destroy / dispose / remove fs object from memory

                    case 1:
                        { 
                        string connectionString = @"data source=ANAMIKA\SQLSERVER;initial catalog=practiceDb;integrated security=true";
                        SqlConnection connection = new SqlConnection(connectionString);
                        SqlCommand command  = new SqlCommand();
                        command.CommandText = "Insert into Employee values('Ajay', 'Delhi',30000, '12/12/2023' ,1)";
                        command .Connection = connection;
                        connection.Open();
                        command.ExecuteNonQuery();
                        connection.Close();
                        command.Dispose();  // This methiod will remove command object
                        connection.Dispose(); // This methiod will remove connection object
                            break;
                        }
                        case 2:
                        {
                            SqlConnection connection = new SqlConnection(@"data source=ANAMIKA\SQLSERVER;initial catalog=practiceDb;integrated security=true");
                            SqlCommand command = new SqlCommand("update employee set address='Bombay' where id=2", connection);
                            connection.Open();
                            command.ExecuteNonQuery();
                            connection.Close();
                            command.Dispose();
                            connection.Dispose();
                            break;
                        }
                    case 3:
                        {
                            SqlConnection connection = new SqlConnection(@"data source=ANAMIKA\SQLSERVER;initial catalog=practiceDb;integrated security=true");
                            SqlCommand command = new SqlCommand("delete Employee where id=2", connection);
                            connection.Open();
                            command.ExecuteNonQuery();
                            connection.Close();
                            command.Dispose();
                            connection.Dispose();
                            break;
                        }
                    case 4:
                        {
                            SqlConnection connection = new SqlConnection(@"data source=ANAMIKA\SQLSERVER;initial catalog=practiceDb;integrated security=true");
                            SqlCommand command = new SqlCommand("select * from employee where id=3", connection);
                            connection.Open();
                            SqlDataReader reader = command.ExecuteReader();
                            if(reader.HasRows)
                            {
                                reader.Read();
                                Console.WriteLine(reader[0] + " "  + reader[1] + " " + reader[2]);

                            }
                            connection.Close();
                            command.Dispose();
                            connection.Dispose();
                            break;
                        }
                    case 5:
                        {
                            SqlConnection connection = new SqlConnection(@"data source=ANAMIKA\SQLSERVER;initial catalog=practiceDb;integrated security=true");
                            SqlCommand command = new SqlCommand("select * from employee", connection);
                            connection.Open();
                            string str = "";
                            SqlDataReader reader = command.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    for (int i=0;i<reader.FieldCount;i++)
                                {                  
                                        str += reader[i] + " ";
                                           
                                    }
                                    Console.WriteLine(str);
                                    str = "";
                                }
                                //while(reader.Read())
                                //{
                                //    Console.WriteLine($"{ reader["id"]}  {reader["name"]}");
                                //}



                            }
                            connection.Close();
                            command.Dispose();
                            connection.Dispose();
                            break;
                        }
                    default: Console.WriteLine("Invalid choice"); break;
                }
            }
        }

        private static int MainMenu()
        {
            Console.WriteLine("1. Insert Record");
            Console.WriteLine("2. Edit Record");
            Console.WriteLine("3. Delete Record");
            Console.WriteLine("1. Search Record");
            Console.WriteLine("1. List all Employees");
            Console.WriteLine("Enter your choice");
            return Byte.Parse(Console.ReadLine());
        }
    }
}